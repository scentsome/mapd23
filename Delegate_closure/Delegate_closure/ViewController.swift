//
//  ViewController.swift
//  Delegate_closure
//
//  Created by Michael on 2018/6/25.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var contentLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        updateLabel(input: "dxxxxx")
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateLabel(input:String) -> Void{
        contentLabel.text = input
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let inputVC = segue.destination as? InputViewController
        inputVC?.callBack = { (input:String) -> Void in
            self.contentLabel.text = input
            
        }
    }


}

