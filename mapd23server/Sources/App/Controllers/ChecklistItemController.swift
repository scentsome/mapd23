//
//  ChecklistItemController.swift
//  App
//
//  Created by Michael on 2018/6/29.
//

import Vapor
struct ChecklistItemController: RouteCollection {
    func boot(router: Router) throws {
        let itemsRoute = router.grouped("api", "items")
        
        itemsRoute.post(ChecklistItem.self, use: createHandler)
        itemsRoute.get(use: getAllHandler)
        itemsRoute.get(ChecklistItem.parameter, use: getHandler)
        itemsRoute.put(ChecklistItem.parameter, use: updateHandler)
        itemsRoute.delete(ChecklistItem.parameter, use: deleteHandler)


    }
    
    func createHandler(_ req: Request, item: ChecklistItem)
        throws -> Future<ChecklistItem> {
            return item.save(on: req)
    }
    
    func getAllHandler(_ req: Request) throws -> Future<[ChecklistItem]> {
        return ChecklistItem.query(on: req).all()
    }
    
    func getHandler(_ req: Request) throws -> Future<ChecklistItem> {
        return try req.parameters.next(ChecklistItem.self)
    }
    
    func updateHandler(_ req: Request) throws -> Future<ChecklistItem> {
        
        return try flatMap(to: ChecklistItem.self,
                        req.parameters.next(ChecklistItem.self),
                           req.content.decode(ChecklistItem.self)) {
                            item, updatedItem in
                            item.content = updatedItem.content
                            item.status = updatedItem.status
                            return item.save(on: req)
        }
    }

    func deleteHandler(_ req: Request)
        throws -> Future<HTTPStatus> {
            
            return try req.parameters
                .next(ChecklistItem.self)
                .delete(on: req)
                .transform(to: HTTPStatus.noContent)
    }

}
