//
//  ViewController.swift
//  ChecklistApp
//
//  Created by Michael on 2018/6/29.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var items = [ChecklistItem]()
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadChecklist()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadChecklist() {
        Alamofire.request("http://localhost:8080/api/items/", method: .get).responseJSON { response in
            print("Request: \(String(describing: response.request))")   // original url request
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)") // original server data as UTF8 string
                let decoder = JSONDecoder()
                do {
                    self.items = try decoder.decode([ChecklistItem].self, from: data)
                    for item in self.items {
                        print(item.content)
                    }
                    self.tableView.reloadData()
                } catch {
                    print(error)
                }
            }
        }
    }
    
    @IBAction func reload(_ sender: Any) {
        reloadChecklist()
    }
    


}

extension ViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        }
        
        cell?.textLabel?.text = items[indexPath.row].content
        cell?.detailTextLabel?.text = items[indexPath.row].status
        return cell!
    }
}

