//
//  ChecklistItem.swift
//  ChecklistApp
//
//  Created by Michael on 2018/6/29.
//  Copyright © 2018 Zencher. All rights reserved.
//

import Foundation
class ChecklistItem: NSObject, Codable {
    var id:String?
    var content = ""
    var status = "false"
    
    func toggleChecked() {
        statusBool = !statusBool
    }
    
    var statusBool: Bool {
        get { return status == "true" }
        set { status = newValue ? "true" : "false" }
    }
}
