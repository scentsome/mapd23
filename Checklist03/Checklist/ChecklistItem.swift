//
//  ChecklistItem.swift
//  Checklist
//
//  Created by Michael on 2018/6/26.
//  Copyright © 2018 Zencher. All rights reserved.
//

import Foundation
class ChecklistItem : NSObject {
    var text = ""
    var checked = false
    func toggleChecked() {
        checked = !checked
    }
}

