//
//  ViewController.swift
//  FirebaseProj
//
//  Created by Michael on 2018/6/29.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    lazy var name:String = String()
    
   
//    func setName(str:String){
//        if name == nil {
//            name = String()
//        }
//        name?.write(str)
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        name.write("ttyy")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func done(segue:UIStoryboardSegue){
    
    }
    
    @IBAction func LoginAction(_ sender: Any) {
        if self.emailTextField.text == "" || self.passwordTextField.text == "" {
            let alertController = UIAlertController(title: "Error",
                                                    message: "Please enter an email and password.",
                                                    preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        } else {
            Auth.auth().signIn(withEmail: self.emailTextField.text!,
                               password: self.passwordTextField.text!)
            { (user, error) in
                if error == nil {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "YoVC")
                    print(user?.additionalUserInfo?.username)
                    self.present(vc!, animated: true, completion: nil)
                } else {
                    let alertController = UIAlertController(title: "Error",
                                                            message: error?.localizedDescription,
                                                            preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
}

