//
//  ImageCollectionViewCell.swift
//  FirebaseProj
//
//  Created by Michael on 2018/6/29.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
}
