//
//  ImageContentViewController.swift
//  FirebaseProj
//
//  Created by Michael on 2018/6/29.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

class ImageContentViewController: UIViewController {
    var fireUploadDic: [String:Any]?

    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let databaseRef = Storage.storage().reference().child("ImageFireUpload")
        
        
//        databaseRef.observe(KeyPath<StorageReference, Value>) { (<#StorageReference#>, <#NSKeyValueObservedChange<Value>#>) in
//            <#code#>
//        }
//        databaseRef.observe(.value, with: { [weak self] (snapshot) in
//
//            print(snapshot.childrenCount)
//            print(snapshot.ref)
//
//            if let uploadDataDic = snapshot.value as? [String:Any] {
//
//                self?.fireUploadDic = uploadDataDic
//                self?.collectionView!.reloadData()
//            }
//        })
        
        print("test...")
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ImageContentViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let dataDic = fireUploadDic {
            
            return dataDic.count
        }
        
        return 0

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ImageCollectionViewCell
 
        if let dataDic = fireUploadDic {
            let keyArray = Array(dataDic.keys)
            if let imageUrlString = dataDic[keyArray[indexPath.row]] as? String {
                if let imageUrl = URL(string: imageUrlString) {
                    URLSession.shared.dataTask(with: imageUrl,
                                               completionHandler: { (data, response, error) in
                                                if error != nil {
                                                    print("Download Image Task Fail: \(error!.localizedDescription)")
                                                }
                                                else if let imageData = data {
                                                    DispatchQueue.main.async {
                                                        cell.imageView.image = UIImage(data: imageData)
                                                    }
                                                }
                    }).resume()
                }
            }
        }
        return cell

    }
    
}
