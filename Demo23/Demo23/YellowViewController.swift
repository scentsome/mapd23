//
//  YellowViewController.swift
//  Demo23
//
//  Created by Michael on 2018/6/25.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit

class YellowViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func returnActions(sender: UIStoryboardSegue){
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("in yellow view controller ")
        print("source ",segue.source)
        print("destination ",segue.destination)
        
    }

}
