//
//  ViewController.swift
//  Demo23
//
//  Created by Michael on 2018/6/25.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate, InputViewControllerDelegate {
    
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var input1: UITextField!
    
    @IBOutlet weak var input2: UITextField!
    
    
    @IBOutlet weak var contentLabel: UILabel!
    
    
    @IBAction func calculate(_ sender: Any) {
        
        let str1:String = input1.text!
        let str2:String = input2.text!
        
        let oValue1:Int? = Int(str1)
        let oValue2:Int? = Int(str2)
        
        
        if oValue1 != nil && oValue2 != nil {
            let val1:Int = oValue1!
            let val2:Int = oValue2!
            let r:Int = val1 + val2
            
            self.resultLabel.text =  "\(r)"
        }else {
            self.resultLabel.text = "請輸入數字"
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.addObserver(forName: NSNotification.Name.UIKeyboardDidShow, object: nil, queue: nil) { (noti:Notification) in
            print("got in white view controller")
        }
    }
    @IBAction func showImagePicker(_ sender: Any) {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        present(imagePicker, animated: true)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(info)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let inputVC = segue.destination as? InputViewController
        inputVC?.delegate = self
        
        
    }
    
    func inputDidEnterText(inputString: String) {
        print(inputString)
        contentLabel.text = inputString
    }

}

