//
//  InputViewController.swift
//  Demo23
//
//  Created by Michael on 2018/6/25.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit

protocol InputViewControllerDelegate {
    func inputDidEnterText(inputString:String)
}

class InputViewController: UIViewController {
    @IBOutlet weak var inputField: UITextField!
    
    var delegate:InputViewControllerDelegate?

    @IBAction func doneAction(_ sender: Any) {
        
        delegate?.inputDidEnterText(inputString: inputField.text!)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func showNextVC(_ sender: Any) {
        let storyboard:UIStoryboard = UIStoryboard(name: "Second", bundle: nil)
        
        let yellowViewController:UIViewController =
            storyboard.instantiateViewController(
                withIdentifier: "YellowViewController")
        
        present(yellowViewController, animated: true) {
            print("Go to Yellow ViewController.")
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.addObserver(forName: NSNotification.Name.UIKeyboardDidShow, object: nil, queue: nil) { (noti:Notification) in
            print("got in gray view controller")
        }
        
        notificationCenter.addObserver(forName: NSNotification.Name.UIKeyboardDidHide, object: nil, queue: nil) { (noti:Notification) in
            print(noti)
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
