//
//  ViewController.swift
//  AlamofireDemo
//
//  Created by Michael on 2018/6/27.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    var a = 9
    var b = 7
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        testGET()
        getMETHOD()
    }
    
    func testGET() {
        let urlString = "https://jsonplaceholder.typicode.com/posts/1"
        Alamofire.request(urlString).response { response in
            
            let html = String(data: response.data!, encoding: .utf8)
            print("\(html)")
        }
    }
    
    func getMETHOD() {
        let jsonURL = "https://jsonplaceholder.typicode.com/posts/1"
        Alamofire.request(jsonURL).responseJSON { (response) in
            do {
                let dict:[String:Any]? = (response.value as? [String:Any])
                print("\(dict!["title"])")
            }catch {
                print("\(error)")
            }
        }
    }
    
    @IBAction func upload(_ sender: Any) {
        let image = UIImage(named:"animal.jpg")
        requestWith(endUrl: "http://localhost:8080/upload", imageData: UIImagePNGRepresentation(image!), parameters: [:])
    }
    func requestWith(endUrl: String, imageData: Data?, parameters: [String : Any] ){
        let url = endUrl
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if let data = imageData{
                multipartFormData.append(data, withName: "image", fileName: "penguin.png", mimeType: "image/png")
            }
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if let err = response.error{
                        print("Error in upload:",err)
                        return
                    }else {
                        print("Succesfully uploaded")
                        
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

