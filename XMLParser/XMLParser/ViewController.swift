//
//  ViewController.swift
//  XMLParser
//
//  Created by Michael on 2018/6/27.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit
import Alamofire
import Kanna

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let xmlString = "<note date=\"2008-01-10\"><to>Tove</to><from>Jani</from></note>"
        
        if let doc = try? Kanna.XML(xml: xmlString, encoding: .utf8){
            for node in doc.xpath("//note/@date"){
                print(node.text)
            }
          
        }
        
        
        
        let xmlURL = "http://data.taipei/opendata/datalist/datasetMeta/download?id=9c9a3f77-8340-48d8-bc0e-f9155521b758&rid=a02ccc34-dd28-4c5d-b527-c5433ec1a453"
        Alamofire.request(xmlURL).responseData { (response) in
            if let data = response.data {
                if let doc = try? Kanna.XML(xml: data, encoding: .utf8) {
                    for node in doc.xpath("//CommonFormat/MAP/ORG_NAME") {
                        
                        print(node.text!)
//                        self.groupName.append(node.text!)
                    }
                    for node in doc.xpath("//ADDRESS") {
                        print(node.text!)
//                        self.addresses.append(node.text!)
                    }
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

