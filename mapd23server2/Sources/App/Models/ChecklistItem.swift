//
//  ChecklistItem.swift
//  App
//
//  Created by Michael on 2018/6/29.
//

import Foundation
import Vapor
import FluentPostgreSQL

final class ChecklistItem: Codable {
    var id: UUID?
    var content: String
    var status: String
    
    init(content: String, status: String) {
        self.content = content
        self.status = status
    }
}
extension ChecklistItem: PostgreSQLUUIDModel {}
extension ChecklistItem: Migration {}
extension ChecklistItem: Content {}
extension ChecklistItem: Parameter {}
