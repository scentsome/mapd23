import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // Basic "Hello, world!" example
    router.get("/api/hello") { req in
       
//        try json.set("name","Michael")
        return "Hello, world"
    }
  
    

    // Example of configuring a controller
//    let todoController = TodoController()
//    router.get("todos", use: todoController.index)
//    router.post("todos", use: todoController.create)
//    router.delete("todos", Todo.parameter, use: todoController.delete)
    
    let checklistItemController = ChecklistItemController()
    try router.register(collection: checklistItemController)
}
