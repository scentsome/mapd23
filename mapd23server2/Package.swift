// swift-tools-version:4.0
import PackageDescription

let package = Package(
    name: "mapd23server",
    dependencies: [
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),
        .package(url: "https://github.com/vapor/fluent-postgresql.git",.exact("1.0.0-rc.3")),

        // 🔵 Swift ORM (queries, models, relations, etc) built on SQLite 3.
     
    ],
    targets: [
        .target(name: "App", dependencies: ["FluentPostgreSQL",  "Vapor"]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)

