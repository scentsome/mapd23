//
//  ParkInfo.swift
//  ParkPlaces
//
//  Created by Michael on 2018/6/27.
//  Copyright © 2018 Zencher. All rights reserved.
//

import Foundation

struct ParkInfo : Codable {
    var result:ParkResult
}

struct ParkResult : Codable {
    var offset:Int
    var limit:Int
    var count:Int
    var sort:String
    var results:[Park]
}

struct Park : Codable {
    var _id:String
    var ParkName:String
    var Name:String
    var YearBuilt:String
    var OpenTime:String
    var Image:String
    var Introduction:String
}
