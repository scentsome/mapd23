//
//  ViewController.swift
//  ParkPlaces
//
//  Created by Michael on 2018/6/27.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var parks:[Park] = [Park]()
    override func viewDidLoad() {
        super.viewDidLoad()
        let jsonURL = "http://data.taipei/opendata/datalist/apiAccess?scope=resourceAquire&rid=bf073841-c734-49bf-a97f-3757a6013812"
        
        Alamofire.request(jsonURL).responseJSON { (response) in
            do {
                let decoder = JSONDecoder()
                let openData = try decoder.decode(ParkInfo.self, from: response.data!)
                self.parks = openData.result.results
                print(openData.result.results[0].Introduction)
                self.tableView.reloadData()
            } catch {
                print("\(error)")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "ParkCell", for: indexPath)
        
        cell.textLabel?.text = parks[indexPath.row].ParkName
        cell.detailTextLabel?.text = parks[indexPath.row].Introduction
        cell.imageView?.image = #imageLiteral(resourceName: "loading.png")
        
        DispatchQueue.global().async {
            let imageURLString:String = self.parks[indexPath.row].Image
            do {
                let imageData:Data = try Data(contentsOf: URL(string:imageURLString)!)
                DispatchQueue.main.async {
                    
                    let localCell = tableView.cellForRow(at: indexPath)
                    print("\(localCell)")
                    localCell?.imageView?.image = UIImage(data: imageData)!
                }
                
            }catch {
                print(error)
            }
        }
        
        
        return cell
        
    }
}
