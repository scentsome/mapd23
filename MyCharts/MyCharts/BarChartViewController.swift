//
//  BarChartViewController.swift
//  MyCharts
//
//  Created by Michael on 2018/6/29.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit
import Charts

class BarChartViewController: UIViewController {
    @IBOutlet weak var barChartView: BarChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        setupLineData()
        barChartView.delegate = self
        
//        let values:[Double] = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0]
//        let lessValues:[String] = values.map { (raw) -> String in
//            return "\(raw)"
//        }
//        print(lessValues)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupLineData(){
        let values:[Double] = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0]
        let lessValues:[Double] = values.map { (raw) -> Double in
            return raw/2
        }
        print(lessValues)
        
        
        var dataEntries:[ChartDataEntry] = []
        
        for (index,value) in lessValues.enumerated() {
            let entry = ChartDataEntry(x: Double(index), y:  Double(value))
            dataEntries.append(entry)
        }
        var dataSet = LineChartDataSet(values: dataEntries, label: "Units Sold")
        dataSet.colors = [UIColor.green]
//        var chartData = LineChartData(dataSets: [dataSet])
        
//        self.barChartView.data = chartData
        
    }
    
    func setupData(){
        let values = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0]
        var dataEntries:[BarChartDataEntry] = []
        
        for (index,value) in values.enumerated() {
            let entry = BarChartDataEntry(x: Double(index), y:  Double(value))
            dataEntries.append(entry)
        }
        let dataSet = BarChartDataSet(values: dataEntries, label: "Units Sold")
        let barChartData = BarChartData(dataSets: [dataSet])
        
        let lessValues:[Double] = values.map { (raw) -> Double in
            return raw/2
        }
        print(lessValues)
        
        
        var dataLineEntries:[ChartDataEntry] = []
        
        for (index,value) in lessValues.enumerated() {
            let entry = ChartDataEntry(x: Double(index), y:  Double(value))
            dataLineEntries.append(entry)
        }
        var dataLineSet = LineChartDataSet(values: dataEntries, label: "Line Units Sold")
        dataSet.colors = [UIColor.green]
        
        
//        barChartData.addDataSet(dataLineSet)
        
        
        self.barChartView.data = barChartData
        self.barChartView.xAxis.labelPosition = .bottom
        let limitLine = ChartLimitLine(limit: 11.0, label: "Target")
        self.barChartView.rightAxis.addLimitLine(limitLine)
    }
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension BarChartViewController: ChartViewDelegate {
    
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        print("Nothing selected")
        
    }
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        print("Did select \(entry)")
        
    }
}
