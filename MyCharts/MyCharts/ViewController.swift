//
//  ViewController.swift
//  MyCharts
//
//  Created by Michael on 2018/6/28.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit
import Charts


extension String {
    func toInt() -> Int? {
        return Int(self)
    }
}

extension Double {
    var km:  Double { return self / 1_000.0 }
    var m:   Double { return self }
    var cm:  Double { return self * 100.0 }
    var mm:  Double { return self * 1_000.0 }
    var ft:  Double { return self / 3.28084 }
}

class ViewController: UIViewController {

    @IBOutlet weak var pieChartView: PieChartView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pieChartView.noDataText = "請輸入資料"
        prepareData()
        pieChartView.delegate = self
    }
    @IBAction func highLight(_ sender: Any) {
        pieChartView.highlightValue(x: 2, y: 0, dataSetIndex: 0)
        pieChartView.rotationAngle = CGFloat (Double.pi / 2)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func prepareData() {
        let pieChartRawData:Dictionary<String,Double> = ["iOS 8":84, "iOS 7":14, "Earlier":2]
        var yValues:[PieChartDataEntry] = []
        
        for (key, obj) in pieChartRawData {
            let entry:PieChartDataEntry = PieChartDataEntry(value:obj,label: key )
            yValues.append(entry)
        }
        let dataSet:PieChartDataSet = PieChartDataSet(values: yValues,
                                                      label: "iOS Distribution")
        var colors:[UIColor] = []
        colors.append(contentsOf: ChartColorTemplates.pastel())
        dataSet.colors = colors
        let pieChartData:PieChartData = PieChartData(dataSet: dataSet)
        pieChartView.data = pieChartData
        pieChartView.chartDescription?.text = "這是測試 Pie Chart"
        pieChartData.setValueTextColor(UIColor.cyan)
    }


}

extension ViewController : ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        print("selected", entry, highlight)
    }
    
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        print("nothing selected")
    }
}
