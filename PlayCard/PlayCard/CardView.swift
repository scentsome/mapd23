//
//  CardView.swift
//  PlayCard
//
//  Created by Michael on 2018/6/28.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit

class CardView: UIImageView {
    var frontImage:UIImage = UIImage()
    var backImage:UIImage = UIImage()
    var isLocked:Bool = false
    var isFront:Bool = false

    func showFront(){
        if isLocked {
            print("locked")
            return
        }
        self.image = frontImage
        self.isFront = true
    }
    
    func showBack(){
        if isLocked {
            return
        }
        self.image = backImage
        self.isFront = false
    }
    
    func sum(a:Int, b:Int) -> Int {
        return a + b
    }
    
    func lock(){
        isLocked = true
        self.layer.borderColor = UIColor.blue.cgColor
        self.layer.borderWidth = 5.0
    }

    func unlock(){
        isLocked = false
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 1.0
    }
    
    func exchangeImage(){
        if isFront {
            showBack()
        }else {
            showFront()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touhed")
        if isLocked {
            self.unlock()
        }else {
            self.lock()
        }
    }
}
