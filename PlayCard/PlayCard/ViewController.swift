//
//  ViewController.swift
//  PlayCard
//
//  Created by Michael on 2018/6/28.
//  Copyright © 2018 Zencher. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var cards: [CardView]!
    
    fileprivate func prepareCardData() {
        for cardView in cards {
            cardView.backImage = UIImage(named: "back")!
            cardView.showBack()
        }
        for (index, cardView) in cards.enumerated() {
            cardView.frontImage = UIImage(named: "front\(index).png")!
            cardView.showFront()
        }
    }
    
    @IBAction func changeCards(_ sender: Any) {
        for cardView in cards {
            cardView.exchangeImage()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareCardData()
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

