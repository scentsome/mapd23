//
//  PlayCardTests.swift
//  PlayCardTests
//
//  Created by Michael on 2018/6/28.
//  Copyright © 2018 Zencher. All rights reserved.
//

import XCTest
@testable import PlayCard

class PlayCardTests: XCTestCase {
    var card:CardView?

    override func setUp() {
        super.setUp()
        card = CardView()
        card?.frontImage = UIImage(named: "front0.png")!
        card?.backImage = UIImage(named: "back.png")!

        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        card = nil
        super.tearDown()
    }
    
    
    func testShowFront(){
        card?.showFront()
        XCTAssertTrue(card?.image === card?.frontImage,
                      "current image should be front image ")
    }

    
    func testShowBack(){
        card?.showBack()
        XCTAssertTrue(card?.image === card?.backImage,
                      "current image should be back image ")
        
    }
    
    func testLock(){
        card?.showFront()
        card?.lock()
        card?.showBack()
        XCTAssertTrue(card?.image === card?.frontImage,
                      "current image should be front image ")
        
    }
    
    func testShowFrontWhenLocked(){
        card?.showBack()
        card?.lock()
        card?.showFront()
        XCTAssertTrue(card?.image === card?.backImage,
                      "current image should be front image ")
    }
    
    func testUnlock(){
        card?.showFront()
        card?.lock()
        card?.unlock()
        card?.showBack()
        XCTAssertTrue(card?.image === card?.backImage,
                      "current image should be front image ")
    }
    
    func testSum(){
        var r = card?.sum(a: 8, b: 9)
        XCTAssertTrue(r == 17 , "should be 17")
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testExchangeImage(){
        card?.showFront()
        card?.exchangeImage()
        XCTAssertTrue(card?.image === card?.backImage,
                      "current image should be front image ")
    }
    
    
}
